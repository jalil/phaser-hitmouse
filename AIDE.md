# Aide

## Documentation

- [Documentation Phaser 3](https://photonstorm.github.io/phaser3-docs)
- [Exemples Phaser 3](http://phaser.io/examples)

## Avec TypeScript

- La doc officielle : https://phaser.io/tutorials/how-to-use-phaser-with-typescript
- Un tuto : https://spin.atomicobject.com/2019/07/13/phaser-3-typescript-tutorial/
- Un dépôt avec pas mal d’exemples : https://github.com/digitsensitive/phaser3-typescript/
- Les fonctions avec TypeScript : https://www.typescriptlang.org/docs/handbook/functions.html

## Pour utiliser les images
```ts
// Au début du fichier
import './assets/toto.png'

// […]

// Dans le preload:
this.load.image('zombie', 'assets/zombie.png')
```

## Aléatoire

Nombre aléatoire entre 5 et 10 : 

```javascript
Phaser.Math.Between(5, 10)
```
