import Phaser from "phaser";
import { HitScene } from "./HitScene";

export class HitTheWhatever {
  private game: Phaser.Game;

  constructor() {
    this.game = new Phaser.Game({
      type: Phaser.AUTO,
      width: 800,
      height: 600,
      transparent: true,
      scene: new HitScene({})
    })
  }
}
