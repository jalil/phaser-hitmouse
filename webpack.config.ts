/* eslint-disable @typescript-eslint/no-var-requires, no-undef */
import {Configuration} from 'webpack';

const path = require('path');
const plugins = require('./webpack/config.plugins');

type BuildMode = 'production' | 'development';

interface ArgV {
  watch?: boolean;
  mode?: BuildMode;
}

interface WebpackEnv {
  analyze?: boolean;
  sentry?: boolean;
}

const webpackConfig = (env: WebpackEnv = {}, argv: ArgV = {}): Configuration => {
  process.env.NODE_ENV = argv.mode;

  return {
    name: 'HitMouse',
    mode: argv.mode,
    resolve: {
      extensions: ['.ts', '.js'],
      modules: [path.resolve(__dirname, 'src'), 'node_modules']
    },
    entry: path.join(__dirname, 'src', 'index.ts'),
    output: {
      filename: 'hitmouse.[hash].js',
      path: path.join(__dirname, 'build'),
    },
    module: { rules: [
        {
          test: /\.(js|ts)$/,
          include: [
            path.resolve(__dirname, 'src')
          ],
          loader: require.resolve('babel-loader')
        },
        {
          test: /\.png$/,
          include: [
            path.resolve(__dirname, 'src')
          ],
          loader: require.resolve('file-loader'),
          options: {
            name: 'assets/[name].[ext]',
          }
        }
      ] },
    plugins: plugins(env, argv),
    devtool: argv.watch ? 'eval-source-map' : 'source-map',
    // @ts-ignore
    devServer: {
      clientLogLevel: 'warn',
      quiet: true,
    },
    stats: "errors-warnings"
  };
};

export { BuildMode, ArgV, WebpackEnv, webpackConfig as default };

module.exports = webpackConfig;
