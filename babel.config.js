// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
module.exports = function (api) {
  api.cache(true)

  return {
    presets: [
      [
        '@babel/env',
        {
          targets: {},
          useBuiltIns: 'usage',
          modules: false,
          corejs: 3
        }
      ],
      '@babel/preset-typescript'
    ],
    plugins: ['transform-class-properties'],
  }
}
