/* eslint-disable @typescript-eslint/no-var-requires */
import {ArgV, WebpackEnv} from '../webpack.config';

const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');


// eslint-disable-next-line @typescript-eslint/ban-types
const webpackConfig = (env: WebpackEnv = {}, argv: ArgV = {}): object[] => {
  const plugins = [
    new ForkTsCheckerWebpackPlugin({
      async: argv.watch,
      // formatter: argv.mode === 'production' ? typescriptFormatter : undefined,
    }),
    new FilterWarningsPlugin({
      exclude: /export .* was not found in/
      // Because of:
      // https://github.com/TypeStrong/ts-loader/issues/751
      // https://github.com/TypeStrong/ts-loader/issues/653
      // https://github.com/webpack/webpack/issues/7378
      // https://github.com/babel/babel/issues/8361
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, '..', 'src', 'index.html'),
      filename: 'index.html',
      inject: true
    })
  ];

  if (argv.watch) {
    plugins.push(
      new CaseSensitivePathsPlugin()
    );
  }

  if (argv.mode === 'production') {
    plugins.push(new CompressionPlugin());
  }

  if (!argv.watch) {
    plugins.push(new CleanWebpackPlugin());
  }

  return plugins;
};

module.exports = webpackConfig;
